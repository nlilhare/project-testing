function validateUserId()
{

	if(!((/\w{3,12}$/).test(document.getElementById("Login_Id").value)))
	{
		$('#error_UserName').html("3-12 characters a-z and A-Z and userscore");
		//document.getElementById("Login_Id").value="";
	}
	return false;
}

function validatePassword()
{
	if(document.getElementById("password").value!=document.getElementById("Confirm_password").value)
	{
		$('#error_password').html("Password Does not match");
		//document.getElementById("password").value="";
		//document.getElementById("Confirm_password").value="";		
	}
	return false;
}

function validateFirstName()
{

	if(!((/[a-zA-Z]$/).test(document.getElementById("First_Name").value)))
	{
		$('#error_FirstName').html("Please enter characters a-z and A-Z");
		//document.getElementById("First_Name").value="";
	}
	return false;
}
function validateLastName()
{
	if(!((/[a-zA-Z]$/).test(document.getElementById("Last_Name").value)))
	{

		$('#error_LastName').html("Please enter characters a-z and A-Z");
		//document.getElementById("Last_Name").value="";
		
	}
	return false;
}
function validateDob()
{
	var dob= Date().split(' ');
	var formattedDate=dob[3]+"-"+dob[2]+"-"+dob[1];

	//dob1=dob.getFullYear()+"-"+(dob.getMonth()+1)+"-"+dob.getDate();
	//console.log(formattedDate);
	document.getElementById("Dob").setAttribute("max", formattedDate);
	return false;

}
function validateEmailId()
{

	if(!(/\S+@\S+\.\S+/.test(document.getElementById("Email_Id").value)))
	{
		
		$('#error_EmailId').html("Inocrrect mail id");
		//document.getElementById("Email_Id").value="";
	}
	return false;
}
function validateMobile()
{

	if(!(/[0-9]{10}$/).test(document.getElementById("Mobile_Number").value))
	{
		$('#error_Mobile').html("Please numbers 0-9 only and 10 digit Number");
		//document.getElementById("Mobile_Number").value="";
	}
	return false;
}
function validateCity()
{
	if(!(/[a-zA-Z]$/).test(document.getElementById("City").value))
	{
		$('#error_City').html("Please Enter characters a-z or A-Z");
		//document.getElementById("City").value="";
	}
	return false;

}
function validatePinCode()
{
	if(!((/[0-9]{6}$/).test(document.getElementById("Pin_Code").value)))
	{
		$('#error_PinCode').html("Please numbers 0-9 only and 6 digit number");
		//document.getElementById("Pin_Code").value="";
	}
	return false;
}
function validateState()
{
	if(!((/[a-zA-Z]$/).test(document.getElementById("State").value)))
	{
		$('#error_State').html("Please enter characters a-z and A-Z");
		///document.getElementById("State").value="";
	}
	return false;
}
function validateCountry()
{
	if(!((/[a-zA-Z]$/).test(document.getElementById("Country").value)))
	{
		$('#error_Country').html("Please enter characters a-z and A-Z");
		//document.getElementById("Country").value="";
	}
	return false;
}
function getGender()
{
	var gender_selection;	
	gender=document.getElementsByName("gender");
	for (i = 0; i<gender.length ;i++) 
	{
		if(gender[i].checked)
		{
			gender_selection=gender[i].value;
			break;
		}
	}
	return gender_selection;

}

function registrationForm()
{


	/*hobby=document.getElementsByName('Hobby');
	var user_hobby= Array(hobby.length);
	for (i = 0; i<hobby.length ;i++) 
	{
		if(hobby[i].checked)
		{
			user_hobby[count++]=hobby[i].value;
		}
	}
	console.log(hobby);
	for(i=count;i<hobby.length;i++)
	{
		user_hobby.pop();
	}*/
	
	$.post('createUserAccount.php',{login_Id:document.getElementById("Login_Id").value,
	password:document.getElementById("password").value,
	first_Name:document.getElementById("First_Name").value,
	last_Name:document.getElementById("Last_Name").value,
	Email_Id:document.getElementById("Email_Id").value,
	Mobile_Number:document.getElementById("Mobile_Number").value,
	gender:getGender(),
	Address:document.getElementById("Address").value,
	City:document.getElementById("City").value,
	Pin_Code:document.getElementById("Pin_Code").value,
	State:document.getElementById("Country").value,
	Country:document.getElementById("Country").value}, function (data) {
			if(data==document.getElementById("First_Name").value)
			{
				console.log("this is here");
				window.location.href="welcome.php";
			}
          
	});	
	return false;
		
}
