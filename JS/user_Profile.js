function edit_profile(loginId)
{
		$.post('fatchUserDetails.php',{login_Id:loginId},function (data)
			{
				var user_details=JSON.parse(data);
				$('#msg').html("");
				document.getElementById("main").style.display="block";
				$('#edit_profile').hide();
				$('#first_name').html(user_details.first_name);
				$('#Last_Name').html(user_details.last_Name);
				$('#Email_Id').val(user_details.Email_Id);
				$('#Mobile_Number').val(user_details.Mobile_Number);
				$('#Address').val(user_details.Address);
				$('#City').val(user_details.City);
				$('#Pin_Code').val(user_details.Pin_Code);
				$('#State').val(user_details.State);
				$('#Country').val(user_details.Country);
				set_gender(user_details.gender);
				

				//do something
			});
		//alert(loginId);
		//console.log(loginId);

}
function set_gender(gender)
{
	if(gender=='Male')
	{
		$('#Male').attr('checked', true);
	}
	else
	{
		$('#Female').attr('checked', true);
	}
}
function saveProfile()
{

	$.post('profile_update.php',{Email_Id:document.getElementById("Email_Id").value,
	Mobile_Number:document.getElementById("Mobile_Number").value,
	gender:getGender(),
	Address:document.getElementById("Address").value,
	City:document.getElementById("City").value,
	Pin_Code:document.getElementById("Pin_Code").value,
	State:document.getElementById("Country").value,
	Country:document.getElementById("Country").value},function(data)
		{
			$('#main').attr("display",'none').hide();
			$('#edit_profile').show();
			$('#msg').html(data);
		});
	return false;
}