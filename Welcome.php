<html>
<head>
	<link rel="stylesheet" type="text/css" href="style/registration.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script type="text/javascript" src="js/user_Profile.js"></script>
	<script type="text/javascript" src="js/Validation.js"></script>
	<script type="text/javascript">
		$(function(){
  		$("#header").load("header.html"); 
  		$("#footer").load("footer.html"); 
		});
	</script>
</head>
<body>
	<div id="header"></div>
	<table>
		<tr><td><h1>
		<?php
		session_start();
		echo $_SESSION['first_Name'];
		//cho $_SESSION['login_Id'];
		?>
		</h1></td><td>&nbsp;&nbsp;&nbsp;&nbsp;
		<input type="button" value="Edit Profile" id="edit_profile" onclick="edit_profile(<?php echo "'".$_SESSION['login_Id']."'"?>);">
		</td></tr>
	</table><p id="msg"></p>	
	<div id="main" style="display: none;">
		<table id="user_table"  >
			<form id="updateProfile" method="post" onsubmit="return saveProfile();">
				<tr><td>First Name </td><td><p id="first_name"></p></td></tr>
				<tr><td>Last Name </td><td><p id="Last_Name"></p></td></tr>
				<tr><td>Email Id </td><td><input type="email" id="Email_Id" maxlength=30 onmouseout="validateEmailId();"><br><p id="error_EmailId" class="errorMsg"></p></td></tr>
				<tr><td>Mobile </td><td><input type="text" id="Mobile_Number"  maxlength="10" onmouseout="validateMobile();">&nbsp;&nbsp;(10 digit number) <br><p id="error_Mobile" class="errorMsg"></p></td></tr>
				<tr><td>Gender</td><td><input type="radio" name="gender" id="Male" value="Male" >Male 
				<input type="radio" name="gender" id="Female" value="Female">Female</td></tr>
				<tr><td>Address </td><td><textarea id="Address" rows="3" cols="20""></textarea> </td></tr>
				<tr><td>City </td><td><input type="text" id="City" maxlength=30 onmouseout="validateCity();">&nbsp;&nbsp;(max characters 30 a-z and A-Z) <br><p id="error_City" class="errorMsg"></p></td></tr>
				<tr><td>Pin Code </td><td><input type="text" id="Pin_Code" maxlength=6 onmouseout="validatePinCode();">&nbsp;&nbsp;(6 digit number) <br><p id="error_PinCode" class="errorMsg"></p></td></tr>
				<tr><td>State </td><td><input type="text" id="State" maxlength=30 onmouseout="validateState();">&nbsp;&nbsp;(max characters 30 a-z and A-Z) <br><p id="error_State" class="errorMsg"></p></td></tr>
				<tr><td>Country </td><td><input type="text" id="Country" maxlength=30 onmouseout="validateCountry();">&nbsp;&nbsp;(max characters 30 a-z and A-Z) <br><p id="error_Country" class="errorMsg"></p></td></tr>
				<tr><td>Hobbies </td><td><input type="checkbox" id="Drawing" name="Hobby" value="Drawing"> Drawing
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="Singing" name="Hobby" value="Singing"> Singing
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="Sketching" name="Hobby" value="Sketching"> Sketching
				&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" id="Other" name="Hobby" value="Other"> Other<br><br></td></tr>
				<tr><td><center><input type="submit" id="Submit" Value="Submit">&nbsp;&nbsp;&nbsp;&nbsp; <input type="reset" id="reset" Value="Reset"></center></td></tr>
			</form>
		</table>
	</div>
<div id="footer"></div>
</body>
</html>>
