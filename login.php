<html>
<head>
    <link rel="stylesheet" type="text/css" href="style/registration.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="js/validateUser.js"></script>
    <script type="text/javascript">
        $(function(){
            $("#header").load("header.html"); 
            $("#footer").load("footer.html"); 
        });

    </script>
</head>
<body>
    <div id="header"></div>
    <br><br>
    <div id="main">
        <form id="loginForm" onsubmit= "return validation();"">
            <center><table width="50%" cellpadding="10px">
                <tr><td>User name</td><td><input type="text" id="userName"></td></tr>
                <tr><td>Password </td><td><input type=password id="password"></td></tr>
                <tr><td><p id="errorMsg" style ="color:red;"></p></td></tr>
                <tr><td colspan="2"><center><input type="submit" id="submit" value="Login"></center></td></tr></tr>
            </table></center>
        </form>
    </div>
    <br><br>
    <div id="footer"></div>
</body>
</html>
