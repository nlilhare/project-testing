-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 09, 2016 at 08:27 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `login`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `login_Id` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `first_Name` varchar(40) NOT NULL,
  `last_Name` varchar(40) NOT NULL,
  `Email_Id` varchar(40) NOT NULL,
  `Mobile_Number` varchar(40) NOT NULL,
  `gender` varchar(40) NOT NULL,
  `Address` varchar(40) NOT NULL,
  `City` varchar(40) NOT NULL,
  `Pin_Code` varchar(40) NOT NULL,
  `State` varchar(40) NOT NULL,
  `Country` varchar(40) NOT NULL,
  `hobby` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`login_Id`, `password`, `first_Name`, `last_Name`, `Email_Id`, `Mobile_Number`, `gender`, `Address`, `City`, `Pin_Code`, `State`, `Country`, `hobby`) VALUES
('23432sdfds', '123', 'tokoshy', '', 'tokoshy@cicso.com', '9999999999', 'Male', 'dsasfd', '', '111111', '', '', ''),
('23525sdfa', '123', 'nasdfdsa', 'dssds', 'nsdsd@gmail.coom', '0989089890', 'Male', 'jljlkj\nk;lk', 'pune', '987987', 'India', 'India', ''),
('343sdasd', '123', 'neha', 'Lilhare', 'neha@gmail.com', '', 'Male', '', '', '', '', '', ''),
('Akashy', '123', 'Akshay', 'pare', 'akki@gmail.com', '9879897879', 'Female', 'sdsfadsf\ndsfads\nfadsf\ndsaf', 'Pune', '988989', 'India', 'India', ''),
('Akashy123', '123', 'Akshay', 'pare', 'akki@gmail.com', '9879897879', 'Female', 'sdsfadsf\ndsfads\nfadsf\ndsaf', 'Pune', '988989', 'India', 'India', ''),
('Amol', '123', 'Amol', 'jain', 'amol@gmail.com', '', 'Male', '', '', '', '', '', ''),
('Amol123', '123', 'Amol', 'jain', 'amol@gmail.com', '9999999999', 'Male', 'sadfads', 'Karnataka', '984534', 'India', 'India', ''),
('anu', '123', 'Anu', 'dhaigude', 'anu@gmial.com', '9988888888', 'Female', 'sda\ndsfdsa\ndsf\nadsf\ndsf\ndsf\ndasf', 'sdfads', '576756', 'India', 'India', ''),
('asasa', '123', 'neha', 'Lilhare', 'neha@gmail.com', '', 'Male', '', '', '', '', '', ''),
('bsdsadb', '123', 'nasdfdsa', 'dssds', 'nsdsd@gmail.coom', '0989089890', 'Male', 'jljlkj\nk;lk', 'pune', '987987', 'India', 'India', ''),
('devika', '123', 'Devika', 'jain', 'devi@jain.com', '9999898898', 'Female', 'dsfasdfds', 'Indore', '777777', 'dsfsaf', 'dsfsaf', ''),
('dsfadsdsfads', '123', 'Neha', 'lilhare', 'neha@gmail.com', '6767867676', 'Female', 'hkjhhkhkhjhh', 'Pune', '774387', 'NehaIndian', 'NehaIndian', ''),
('dsfdsfds', '123', 'dfdsfads', 'dsfdsaf', 'nehalilhare@gmail.com', '3444444444', 'Female', 'sdsf\ndfds\ndsf\ndsaf', 'Pune', '343434', 'India', 'India', ''),
('Gurpreeet', '123', 'Gur', 'preet', 'gur@preet.com', '4574574574', 'Female', 'asdasfasd', 'Indore', '234324', 'India', 'India', ''),
('Gurpreeet123', '123', 'Gur', 'preet', 'gur@preet.com', '4574574574', 'Female', 'asdasfasd', 'Indore', '234324', 'India', 'India', ''),
('harish', '1234', 'Harish', 'jain', 'hari@jain.com', '4365765865', 'Male', 'dsfasd', 'indore', '452009', 'indore', 'indore', ''),
('Mashu123', '123', 'MAdhu', 'Mane', 'mnane@gmail.com', '4564653653', 'Female', 'sdfads', 'Pune', '453645', 'India', 'India', ''),
('MOm', '123', 'Mummy', 'tsadfds', 'mnane@gmail.com', '8999999999', 'Male', 'dsafads', 'Indore', '433333', 'indai', 'indai', ''),
('Neha', '123', 'Neha', 'Lilhare', 'neha@gmail.com', '4363333333', 'Male', 'adfdsafc', 'Pune', '411014', 'India', 'India', ''),
('neha1121', '123', 'Rahul', 'Sharma', 'rsharma@cisco.com', '7887878787', 'Female', 'sdfas', 'sdfads', '666666', 'sadadad', 'sadadad', ''),
('neha1128', '123', 'Neha', 'Lilhare', 'nlilhare@cisco.com', '8888888888', 'Female', 'sdfas', 'sdfads', '666666', 'sadadad', 'sadadad', ''),
('Neha1234', '1234', 'Neha', 'Lilhare', 'nehalilhare@gmail.com', '4364574574', 'Female', 'sdfsadjfjdsklf', 'Indore', '324322', 'england', 'england', ''),
('NehaMalviya', '123', 'Neha', 'Malviya', 'neha@gmail.com', '8799999999', 'Female', 'sdfadsfasdfdsa', 'Pune', '873429', 'India', 'India', ''),
('neharahul', '123', 'neha rahul', '', '', '', 'Male', '', '', '', '', '', ''),
('nnnnn', '123', 'nnnnnnnnnnn', '', '', '9999999999', 'Male', '', '', '', '', '', ''),
('nsharma', '123', 'Neha', 'Sharma', 'nsharma@gmail.com', '6656787987', 'Female', 'sdfasd\nads\nsd', 'Indore', '534324', 'India', 'India', ''),
('Papiya', '123', 'Papiya', 'banerjee', 'pban@cisco.com', '3284798327', 'Female', 'sdfadsfds\ndfsdf\n', 'Pune', '654433', 'India', 'India', ''),
('Payal', '123', 'Payal', 'Pawade', 'ppawade@gmail.com', '7383828383', 'Female', 'Nehasdfasd', 'Pune', '411014', 'India', 'India', ''),
('piyu', '123', 'Piyu', 'Piyu', 'piyu@piyu.com', '9999999999', 'Female', 'dsfad', 'pune', '999999', 'saasass', 'saasass', ''),
('Pramit', '123', 'Pramit', 'gaurav', 'pgurav@cisco.com', '4357438979', '', 'sdfasd', 'mumbai', '434532', 'India', 'India', ''),
('qweqe121', '123', 'sdfadsf', 'lilhare', 'neha@gmail.com', '', 'Male', '', '', '', '', '', ''),
('rahul1', '123', 'rahul', '', '', '', 'Male', '', '', '', '', '', ''),
('rahul11', '123', 'rahul', '', '', '', 'Male', '', '', '', '', '', ''),
('rahul123', '123', 'rahul', '', '', '', 'Male', '', '', '', '', '', ''),
('rasdsdf', '123', 'nasdfsd', 'sdfcas', 'mnane@gmail.com', '4365346465', 'Female', 'dsfads', 'Pune', '898787', 'India', 'India', ''),
('rer1', '123', 'sdfafas', 'sdfasdfs', 'baby.singh@gmail.com', '7777777777', 'Male', 'dsafff', 'sdfasd', '456767', 'India', 'India', ''),
('sdsa11111111', '123', 'Neha', '', 'lilhare@gmail.com', '1111111111', 'Male', '', '', '', '', '', ''),
('sdsa23412342', '123', 'dsafadsf', '', '', '', 'Female', '', '', '', '', '', ''),
('sdsa456', '123', 'sdfdsaa', 'sdfas', 'akki@gmail.com', '9999999999', 'Female', 'sdfads', 'sfda', '123213', 'sadfdasf', 'sadfdasf', ''),
('sdsa988', '123', 'dsfcadsfas', '', 'neha@gmail.com', '8789999999', 'Female', 'sdas', '', '', '', '', ''),
('Simi', '123', 'Simmi', 'Chawla', 'chawla@gmail.com', '0898989898', 'Female', 'sdfasd\nsdfa', 'Pune', '775656', 'India', 'India', ''),
('test', '123', 'Neha', 'lilhare', 'n@gmail.com', '1111111111', 'Male', '1100', 'indore', '444444', 'pune', 'pune', ''),
('test11', '123', 'Neha', 'test', 'test@neha.com', '4574545745', 'Male', 'sdfasfsd', 'dsfsd', '666666', 'sda', 'sda', ''),
('tetse12', '123', 'dsfdsf', 'sdfasdfs', 'sonu@gmail.com', '7666666666', 'Female', 'erfqafads', 'sdfads', '444444', 'ewa', 'ewa', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`login_Id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
